package com.virtusa.SkyScanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class SonarJSON {

    public static void main(String[] args) throws IOException {

        String apiUrl = "http://localhost:9098/api/issues/search?"
        		+ "componentKeys=skyscanner&resolved=false&"
        		+ "types=BUG,CODE_SMELL,VULNERABILITY"
        		+ "&ps=500&additionalFields=comments";
        URL url = new URL(apiUrl);


        String username = "admin";
        String password = "root";
        String authString = username + ":" + password;
        String authHeader = "Basic " + Base64.getEncoder().encodeToString(authString.getBytes(StandardCharsets.UTF_8));


        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Authorization", authHeader);
        connection.setRequestProperty("Accept", "application/json");

  
        if (connection.getResponseCode() >= 400) {
            System.out.println("Error: " + connection.getResponseCode() + " " + connection.getResponseMessage());
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            String errorOutput;
            while ((errorOutput = errorReader.readLine()) != null) {
                System.out.println(errorOutput);
            }
        } else {
            // Read the JSON response from the API endpoint
            BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
            StringBuilder sb = new StringBuilder();
            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }

            // Print the JSON response to the console
            System.out.println(sb.toString());

            // Close the connection and release any resources used
            connection.disconnect();
            br.close();
        }
    }

}

package com.virtusa.SkyScanner.model;

public class Bug {
	
	private String  key;
	private String severity;
	private String message;
	private String component;
	private int line;
	
	
	
	public Bug() {
		super();
	}
	public Bug(String key, String severity, String message, String component, int line) {
		super();
		this.key = key;
		this.severity = severity;
		this.message = message;
		this.component = component;
		this.line = line;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	@Override
	public String toString() {
		return "Bug [key=" + key + ", severity=" + severity + ", message=" + message + ", component=" + component
				+ ", line=" + line + "]";
	}
	
}

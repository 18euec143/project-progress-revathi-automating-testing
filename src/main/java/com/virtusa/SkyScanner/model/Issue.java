package com.virtusa.SkyScanner.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



public class Issue {
	private String key;
	private String severity;
	private String message;
	private String component;
	private Integer line;
	private String type;
	public Issue(String key, String severity, String message, String component, Integer line, String type) {
		super();
		this.key = key;
		this.severity = severity;
		this.message = message;
		this.component = component;
		this.line = line;
		this.type = type;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public Integer getLine() {
		return line;
	}
	public void setLine(Integer line) {
		this.line = line;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "Issue [key=" + key + ", severity=" + severity + ", message=" + message + ", component=" + component
				+ ", line=" + line + ", type=" + type + "]";
	}
	
	
	
}

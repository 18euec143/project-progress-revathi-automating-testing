package com.virtusa.SkyScanner.model;

public class Measures {
	private String metric;
	private String value;
	private String bestValue;
	public Measures(String metric, String value, String bestValue) {
		super();
		this.metric = metric;
		this.value = value;
		this.bestValue = bestValue;
	}
	
	
	public String getMetric() {
		return metric;
	}


	public void setMetric(String metric) {
		this.metric = metric;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getBestValue() {
		return bestValue;
	}


	public void setBestValue(String bestValue) {
		this.bestValue = bestValue;
	}


	@Override
	public String toString() {
		return "Measures [metric=" + metric + ", value=" + value + ", bestValue=" + bestValue + "]";
	}
	
	public Measures () {}
	
}

package com.virtusa.SkyScanner.model;

import java.util.List;

public class Issues {

	private List<Issue> issues;

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}
	
}

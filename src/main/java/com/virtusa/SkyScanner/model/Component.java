package com.virtusa.SkyScanner.model;

import java.util.List;

public class Component {
	
	private String key;
	private String name;
	private String description;
	private String qualifier;
	private List<Measures> measures;

	
	
	public Component(String key, String name, String description, String qualifier, List<Measures> measures) {
		super();
		this.key = key;
		this.name = name;
		this.description = description;
		this.qualifier = qualifier;
		this.measures = measures;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQualifier() {
		return qualifier;
	}
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}
	public List<Measures> getMeasures() {
		return measures;
	}
	public void setMeasures(List<Measures> measures) {
		this.measures = measures;
	}
	@Override
	public String toString() {
		return "Component [key=" + key + ", name=" + name + ", description=" + description + ", qualifier=" + qualifier
				+ ", measures=" + measures + "]";
	}
	
	public Component() {}
}

package com.virtusa.SkyScanner.controllerImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.virtusa.SkyScanner.model.Component;
import com.virtusa.SkyScanner.model.Components;
import com.virtusa.SkyScanner.model.Issue;
import com.virtusa.SkyScanner.model.Issues;

@RestController
public class CodeSmellsController {

	@Autowired
	private final RestTemplate restTemplate;

	public CodeSmellsController(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}

	@CrossOrigin(origins = "http://localhost:3000", methods = RequestMethod.GET)
	@GetMapping("/codesmells")
	public List<Issue> getCodeSmells() {

		String url = "http://localhost:9098/api/issues/search?" + "componentKeys=skyscanner&resolved=false&"
				+ "types=CODE_SMELL" + "&ps=500&additionalFields=comments";
		ResponseEntity<Issues> responseEntity = restTemplate.getForEntity(url, Issues.class);
		Issues issues = responseEntity.getBody();
		return issues.getIssues();

	}
	
//	@CrossOrigin(origins = "http://localhost:3000", methods = RequestMethod.GET)
	@GetMapping("/codecoverage")

	public ResponseEntity<Object> getCodeCoverage() throws JsonProcessingException {

	String url ="http://localhost:9098/api/measures/component?component=skyscanner&metricKeys=coverage,violations,complexity,ncloc,tests,test_errors";

	ResponseEntity<Object> responseEntity = restTemplate.getForEntity(url, Object.class);

	//Component component = responseEntity.getBody();

	return responseEntity;

	//Component component = responseEntity.getBody();

//	return responseEntity;
//		 ObjectMapper objectMapper = new ObjectMapper(); 
//		 ResponseEntity<Object> responseEntity = restTemplate.getForEntity(url, Object.class); 
//		 String json = objectMapper.writeValueAsString(responseEntity); 
//		return json;

	}
	
	
	
	
	

	@CrossOrigin(origins = "http://localhost:3000", methods = RequestMethod.GET)
	@GetMapping("/bugs")

	public List<Issue> getBugs() {

		String url = "http://localhost:9098/api/issues/search?componentKeys=skyscanner&resolved=false&types=BUG";

		ResponseEntity<Issues> responseEntity = restTemplate.getForEntity(url, Issues.class);

		if (responseEntity.hasBody()) {

			Issues issues = responseEntity.getBody();

			return issues.getIssues();

		}

		return null;

	}

	@CrossOrigin(origins = "http://localhost:3000", methods = RequestMethod.GET)
	@GetMapping("/vulnerabilities")

	public List<Issue> getVulnerabilities() {

		String url = "http://localhost:9098/api/issues/search?componentKeys=skyscanner&resolved=false&types=VULNERABILITY";

		ResponseEntity<Issues> responseEntity = restTemplate.getForEntity(url, Issues.class);

		if (responseEntity.hasBody()) {

			Issues issues = responseEntity.getBody();

			return issues.getIssues();

		}

		return null;

	}

}
